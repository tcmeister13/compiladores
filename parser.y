%{
/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include <comp_dict.h>
#define IKS_SIMBOLO_INDEFINIDO 0
#define IKS_SIMBOLO_LITERAL_INT 1
#define IKS_SIMBOLO_LITERAL_FLOAT 2
#define IKS_SIMBOLO_LITERAL_CHAR 3
#define IKS_SIMBOLO_LITERAL_STRING 4
#define IKS_SIMBOLO_LITERAL_BOOL 5
#define IKS_SIMBOLO_IDENTIFICADOR 6
#define RS_ERRO 1
#define RS_SUCESSO 0
extern int linha;
extern comp_dict_t* dicionario;
extern char* yytext;
char *sem_ultimo;
char *sem_primeiro;
int tipo_id;
%}
/* Declaração dos tokens da gramática da Linguagem K */
%token TK_PR_INT
%token TK_PR_FLOAT
%token TK_PR_BOOL
%token TK_PR_CHAR
%token TK_PR_STRING
%token TK_PR_IF
%token TK_PR_THEN
%token TK_PR_ELSE
%token TK_PR_WHILE
%token TK_PR_DO
%token TK_PR_INPUT
%token TK_PR_OUTPUT
%token TK_PR_RETURN
%token TK_OC_LE
%token TK_OC_GE
%token TK_OC_EQ
%token TK_OC_NE
%token TK_OC_AND
%token TK_OC_OR
%token TK_LIT_INT
%token TK_LIT_FLOAT
%token TK_LIT_FALSE
%token TK_LIT_TRUE
%token TK_LIT_CHAR
%token TK_LIT_STRING
%token TK_IDENTIFICADOR
%token TOKEN_ERRO
%start s
%%
 /* Regras (e ações) da gramática da Linguagem K */

s 		:	var s { return RS_SUCESSO;}
			| func  s {return RS_SUCESSO;}
			| error 	{  	
						//printf("Erro linha %d",linha);
			                        yyerrok ;
			                        yyclearin ;
						return RS_ERRO;
				   	}
			| /*  empty */ {return RS_SUCESSO;};

tipo		:	TK_PR_INT { tipo_id = IKS_SIMBOLO_LITERAL_INT;}| TK_PR_FLOAT { tipo_id = IKS_SIMBOLO_LITERAL_FLOAT;} | TK_PR_BOOL { tipo_id = IKS_SIMBOLO_LITERAL_BOOL;} 
			| TK_PR_CHAR  { tipo_id = IKS_SIMBOLO_LITERAL_CHAR;} | TK_PR_STRING { tipo_id = IKS_SIMBOLO_LITERAL_STRING;};

tipoliteral	: 	TK_LIT_INT { dicionario = dicionario_AdicionaTipo(yytext,IKS_SIMBOLO_LITERAL_INT,dicionario);} 
			| TK_LIT_FLOAT {dicionario = dicionario_AdicionaTipo(yytext,IKS_SIMBOLO_LITERAL_FLOAT,dicionario);} 
			| TK_LIT_CHAR {sem_ultimo = (char*)malloc(strlen(yytext)-1); strncpy(sem_ultimo,yytext,strlen(yytext)-1); sem_primeiro = &sem_ultimo[1]; dicionario = dicionario_AdicionaTipo(sem_primeiro,IKS_SIMBOLO_LITERAL_CHAR,dicionario);} 
			| TK_LIT_STRING {sem_ultimo = (char*)malloc(strlen(yytext)-1); strncpy(sem_ultimo,yytext,strlen(yytext)-1); sem_primeiro = &sem_ultimo[1]; dicionario = dicionario_AdicionaTipo(sem_primeiro,IKS_SIMBOLO_LITERAL_STRING,dicionario);} 
			| TK_LIT_TRUE {dicionario = dicionario_AdicionaTipo(yytext,IKS_SIMBOLO_LITERAL_BOOL,dicionario);} 
			| TK_LIT_FALSE {dicionario = dicionario_AdicionaTipo(yytext,IKS_SIMBOLO_LITERAL_BOOL,dicionario);} 

tipoid		:	TK_IDENTIFICADOR { dicionario = dicionario_AdicionaTipo(yytext,tipo_id,dicionario);};
vetor		:	';' 

var 		:	tipo ':' tipoid  vetor 
			| '[' TK_LIT_INT ']' ';' ;



func		:	cabecalho declaracoes corpo ;


declaracoes	:	/* empty */
			| tipo ':' tipoid ';' declaracoes ;


cabecalho	:	tipo ':' tipoid '(' param ;
param		:	')' 
			| comparam ;

comparam	:	tipo ':' tipoid ')' 
			| tipo ':' tipoid ',' comparam ;



corpo		:	'{' comando '}' ;

comando		:	/* empty */ 
			| comandoextra
			| ';' ;
comandoextra	:	atribuicao comandomais 
			| fluxocontrole comandomais 
			| entrada comandomais 
			| saida comandomais 
			| retorno comandomais 
			| chamadafunc comandomais
			| '{'   comando '}'  comandomais;
comandomais	:	/* empty */ 
			| ';'  comando;

atribuicao	:	tipoid '=' expressao 
			| tipoid '[' expressao ']' '=' expressao  ;

entrada		: 	TK_PR_INPUT tipoid;

saida		:	TK_PR_OUTPUT  elementosaida;
elementosaida	:	TK_LIT_STRING listasaida
			| aritlog listasaida;

listasaida	:	',' elementosaida
			| /* empty */;

retorno		:	TK_PR_RETURN expressao ;

fluxocontrole	:	TK_PR_IF '(' expressao ')' TK_PR_THEN comando 
			| TK_PR_IF '(' expressao ')' TK_PR_THEN comando TK_PR_ELSE comando
			| TK_PR_WHILE '(' expressao ')' TK_PR_DO comando
			| TK_PR_DO comando TK_PR_WHILE '(' expressao ')';
		

expressao	: 	aritlog;

aritlog 	:	'(' aritlog ')' 
			| aritlog operadorarit aritlog 
			| aritlog operadorlog aritlog 
			| aritlog operadorrel aritlog 
			| '-' aritlog
			| '!' aritlog 
			| acessoelemento ;

operadorarit	:	'+' 
			| '-' 
			| '*' 
			| '/';
			
		
operadorlog	: 	TK_OC_OR 
			| TK_OC_AND;

operadorrel 	: 	TK_OC_LE | TK_OC_GE | TK_OC_EQ | TK_OC_NE | '<' | '>' ;
	

acessoelemento 	: 	tipoid acessovetor 
			| tipoliteral 
			| tipoid '(' argumentos ;

argumentos	:	')' 
			| comarg;

comarg		:	acessoelemento ')'
			| acessoelemento ',' comarg;

acessovetor	:	/* empty */
			| '[' expressao ']' ;

chamadafunc	:	acessoelemento;

%%

