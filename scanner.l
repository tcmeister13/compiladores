%{
#include "parser.h"
#include "string.h"
#include "comp_dict.h"
int coment_flag = 0;
int linha = 1; 
char *sem_ultimo;
char *sem_primeiro;
comp_dict_t* dicionario = NULL;
%}
%x comment
%x string
DIGITO [0-9]
MINUSCULA [a-z]
MAIUSCULA [A-Z]
SUBLINHADO [_]
%%
int { return TK_PR_INT; }
float { return TK_PR_FLOAT; }
bool { return TK_PR_BOOL; }
char { return TK_PR_CHAR; }
string { return TK_PR_STRING; }
if { return TK_PR_IF; }
then { return TK_PR_THEN; }
else { return TK_PR_ELSE; }
while { return TK_PR_WHILE; }
do { return TK_PR_DO; }
input { return TK_PR_INPUT; }
output { return TK_PR_OUTPUT; }
return { return TK_PR_RETURN; }
"//".* { }
"/*"		BEGIN(comment);
<comment>[^*\n]*	  
<comment>"*"+[^*/\n]*   
<comment>\n		   ++linha;
<comment>"*"+"/"	   BEGIN(INITIAL);
"," { return yytext[0]; }
";" { return yytext[0]; }
":" { return yytext[0]; }
"(" { return yytext[0]; }
")" { return yytext[0]; }
"[" { return yytext[0]; }
"]" { return yytext[0]; }
"{" { return yytext[0]; }
"}" { return yytext[0]; }
"+" { return yytext[0]; }
"-" { return yytext[0]; }
"*" { return yytext[0]; }
"/" { return yytext[0]; }
"<" { return yytext[0]; }
">" { return yytext[0]; }
"=" { return yytext[0]; }
"!" { return yytext[0]; }
"&" { return yytext[0]; }
"$" { return yytext[0]; }
"<=" { return TK_OC_LE; }
">=" { return TK_OC_GE; }
"==" { return TK_OC_EQ; }
"!=" { return TK_OC_NE; }
"&&" { return TK_OC_AND; }
"||" { return TK_OC_OR; }
"true" { dicionario = dicionario_AdicionarEntrada (yytext,linha,dicionario); return TK_LIT_TRUE; }
"false" { dicionario = dicionario_AdicionarEntrada (yytext,linha,dicionario); return TK_LIT_FALSE; }
({MINUSCULA}|{MAIUSCULA}|{SUBLINHADO})+(({MINUSCULA}|{MAIUSCULA}|{SUBLINHADO})|{DIGITO})* { dicionario = dicionario_AdicionarEntrada (yytext,linha,dicionario); return TK_IDENTIFICADOR; }
"-"?{DIGITO}+ { dicionario = dicionario_AdicionarEntrada (yytext,linha,dicionario); return TK_LIT_INT; }
"-"?{DIGITO}+"."{DIGITO}+ { dicionario = dicionario_AdicionarEntrada (yytext,linha,dicionario); return TK_LIT_FLOAT; }
"'"."'" { sem_ultimo = (char*)malloc(strlen(yytext)-1); strncpy(sem_ultimo,yytext,strlen(yytext)-1); sem_primeiro = &sem_ultimo[1]; dicionario = dicionario_AdicionarEntrada (sem_primeiro,linha,dicionario); return TK_LIT_CHAR;}
\"[^"]*\" { sem_ultimo = (char*)malloc(strlen(yytext)-1); strncpy(sem_ultimo,yytext,strlen(yytext)-1); sem_primeiro = &sem_ultimo[1]; dicionario = dicionario_AdicionarEntrada (sem_primeiro,linha,dicionario);  return TK_LIT_STRING; }
" " {  }
"\t" { }
\n {linha++;} 
. {  return TOKEN_ERRO; }
%%

