/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "comp_dict.h"


/**
 * Cria um dicionário
 * name: dicionario_Criar
 * @param
 * @return NULL
 *
 */

comp_dict_t* dicionario_Criar (){
	return NULL;
}

/**
 * Verifica se chave não é existente no dicionário
 * name: dicionario_chave_nao_existente
 * @param Chave: chave a ser buscada
 * @param Dicionario: onde a chave deve ser buscada
 * @return 1 se não existe; 0 se existe
 *
 */

int dicionario_chave_nao_existente(char* Chave, comp_dict_t* Dicionario)
{
	comp_dict_t* aux = Dicionario;
	int nao_achou = 1;
	while(aux!=NULL && nao_achou==1)
	{
		if(strcmp(aux->chave, Chave)==0)
		{
			nao_achou = 0;
			return nao_achou;
		}

		aux=aux->prox;
	}

	return nao_achou;
}
/**
 * Adiciona nova entrada no dicionário
 * name: dicionario_AdicionarEntrada
 * @param Chave: chave da nova entrada
 * @param dado: informação da entrada
 * @param Dicionario: diconário no qual deve ser adicionado a entrada
 * @return Dicionario atualizado
 *
 */

comp_dict_t* dicionario_AdicionarEntrada (char* Chave, int dado, comp_dict_t* Dicionario){

	   comp_dict_t *novo;
       comp_dict_t *ant = NULL; //ponteiro auxiliar para a posição anterior
       comp_dict_t *ptaux = Dicionario; //ponteiro auxiliar para percorrer a lista


	   /*aloca um novo nodo */
	   novo = (comp_dict_t*) malloc(sizeof(comp_dict_t));

	   /*insere a entrada no novo nodo*/
	   novo->chave = (char*) malloc(strlen(Chave));
	   strncpy(novo->chave,Chave,strlen(Chave));
	   novo->dado = dado;
	   novo->tipo = 0;
	   
	   
	   /*procurando a posição de inserção*/
	   while ((ptaux!=NULL) && (strcmp(ptaux->chave, novo->chave)<0) && (strcmp(ptaux->chave, novo->chave)!=0)) //se info.titulo < dados.titulo então strcmp retorna um valor menor que zero
	   {
			 ant = ptaux;
			 ptaux = ptaux->prox;
	   }
	   
	   //chave já existe
	   if((ptaux!=NULL) && (strcmp(ptaux->chave,novo->chave)==0))
	   {
		   //printf("Chave ja existente trocando linha\n");
		   ptaux->dado = dado;
		   
		   free(novo);
	   }
	   else
	   {
		   /*encadeia o elemento*/
		   if (ant == NULL) /*o anterior não existe, logo o elemento será inserido na primeira posição*/
		   {
				 //  puts("inserindo primeiro");
				   novo->prox = Dicionario;
				   Dicionario = novo;
		   }
		   else /*elemento inserido no meio da lista*/
		   {
				novo->prox = ant->prox;
				ant->prox = novo;
		   }
	   }

       return Dicionario;
}

/**
 * Remove uma entrada do dicionário
 * name: dicionario_RemoverEntrada
 * @param Chave: chave da entrada a ser removida
 * @param Dicionario: Dicionario no qual deve ser removida a entrada
 * @return Dicionario atualizado
 *
 */

comp_dict_t* dicionario_RemoverEntrada (char* Chave, comp_dict_t* Dicionario){
     comp_dict_t *ant = NULL; //ponteiro auxiliar para a posição anterior
     comp_dict_t *ptaux = Dicionario; //ponteiro auxiliar para percorrer a lista

     /*procura o elemento na lista*/
     while (ptaux !=NULL && (strcmp(ptaux->chave, Chave)))
     {
          ant = ptaux;
          ptaux = ptaux->prox;
     }

     /*verifica se achou*/
     if (ptaux == NULL)
       return Dicionario; /*retorna a lista original*/

    if (ant == NULL) /*vai remover o primeiro elemento*/
      Dicionario = ptaux->prox;
    else /*vai remover do meio ou do final*/
      ant->prox = ptaux->prox;

    free(ptaux);

    return Dicionario;
}

/**
 * Altera a info de uma entrada do dicionário
 * name: dicionario_AlterarEntrada
 * @param Chave: chave da entrada a ser alterada
 * @param dado: informação a ser colocada
 * @param Dicionario: dicionário na qual está a entrada a ser alterada
 * @return Dicionario atualizado
 *
 */

comp_dict_t* dicionario_AlterarEntrada (char* Chave, int dado, comp_dict_t* Dicionario){
	 comp_dict_t *ant = NULL; //ponteiro auxiliar para a posição anterior
     comp_dict_t *ptaux = Dicionario; //ponteiro auxiliar para percorrer a lista

     /*procura o elemento na lista*/
     while (ptaux !=NULL && (strcmp(ptaux->chave, Chave)<0))
     {
          ant = ptaux;
          ptaux = ptaux->prox;
     }

     /*verifica se achou*/
     if (ptaux == NULL)
       return Dicionario; /*retorna a lista original*/
     else 
       ptaux->dado = dado;

     return Dicionario;
}


/**
 * Retorna a entrada do dicionário
 * name: dicionario_AlterarEntrada
 * @param Chave: chave da entrada solicitada
 * @param Dicionario: dicionário na qual está a entrada
 * @return Entrada da chave
 *
 */

comp_dict_t* dicionario_LocalizarEntrada (char* Chave, comp_dict_t* Dicionario){
	 comp_dict_t *ant = NULL; //ponteiro auxiliar para a posição anterior
     comp_dict_t *ptaux = Dicionario; //ponteiro auxiliar para percorrer a lista

     /*procura o elemento na lista*/
     while (ptaux !=NULL && (strcmp(ptaux->chave, Chave)<0))
     {
          ant = ptaux;
          ptaux = ptaux->prox;
     }

     /*verifica se achou*/
     if (ptaux == NULL)
       return NULL; /*não achou retorna NULL*/

     return ptaux; /*retorna a entrada */
}


/**
 * Adiciona o tipo para um elemento
 * name: dicionario_AdicionaTipo
 * @param Chave: chave da entrada a ser alterada
 * @param tipo: tipo do elemento da chave
 * @param Dicionario: dicionário na qual está a entrada a ser alterada
 * @return Dicionario atualizado
 *
 */
comp_dict_t* dicionario_AdicionaTipo (char* Chave, int tipo, comp_dict_t* Dicionario){
	 comp_dict_t *ant = NULL; //ponteiro auxiliar para a posição anterior
     comp_dict_t *ptaux = Dicionario; //ponteiro auxiliar para percorrer a lista
	//printf("procurado: %s\n",Chave);
     /*procura o elemento na lista*/
     while (ptaux !=NULL && (strcmp(ptaux->chave, Chave)<0))
     {
	//printf("Atual: %s\n",ptaux->chave);
          ant = ptaux;
          ptaux = ptaux->prox;
     }

     /*verifica se achou*/
     if (ptaux == NULL)
	{
	//printf("Aaaaa");
       return Dicionario; /*retorna a lista original*/
	}
     else
	{
	ptaux->tipo = tipo;
	//printf("%s - %d\n",ptaux->chave, ptaux->tipo);		
	
	}
       

     return Dicionario;

}

/**
 * Mostra o dicionário na tela
 * name: dicionario_imprime
 * @param dicio: Dicionário a ser impresso na tela
 * @return
 *
 */

void dicionario_imprime(comp_dict_t* dicio)
{
	comp_dict_t* aux = dicio;
	printf("IMPRIMINDO DICIONARIO\n");
	while(aux!=NULL)
	{
		printf("\nEntrada chave: %s\n", aux->chave);
		printf("Entrada informacao: %d\n", aux->dado);
		printf("Tipo chave: %d\n",aux->tipo);

		aux = aux->prox;
	}
	printf("\nFIM DICIONARIO\n\n");
}
