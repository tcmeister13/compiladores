/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include <stdlib.h>
#include "comp_graph.h"

/**
 * Cria novo grafo
 * name: grafo_criacao
 * @param 
 * @return NULL
 * 
 */
comp_graph_t* grafo_criacao()
{
	return NULL;	
}

/**
 * Remove da lista de vizinhos um nodo já excluído
 * name: grafo_remove_vizinho
 * @param grafo: onde deve se alterar os vizinhos
 * @param id_nodo_remover: id do nodo a ser removido nos vizinhos
 * @param vizinhos_nodo_remover: lista de ids dos vizinhos do id removido
 * @return Grafo atualizado
 * 
 */
comp_graph_t* grafo_remove_vizinho(comp_graph_t* grafo, int id_nodo_remover, Vizinho* vizinhos_nodo_remover)
{
	comp_graph_t* grafo_aux = grafo;
	
	
	while(vizinhos_nodo_remover!=NULL)
	{
		grafo_aux = grafo;
		//procura nodo que era vizinho do removido
		int id_vizinho = vizinhos_nodo_remover->id_nodo;
		
		
		while(grafo_aux!= NULL && grafo_aux->id_nodo!=id_vizinho)
		{
			grafo_aux = grafo_aux->prox_nodo;
		}
		
		Vizinho* vizinhos_aux = grafo_aux->vizinhos;
		
		//procura por nodo que foi removido
		Vizinho* vizinhos_ant=NULL;
		while(vizinhos_aux!=NULL && vizinhos_aux->id_nodo!=id_nodo_remover)
		{
			vizinhos_ant=vizinhos_aux;
			vizinhos_aux=vizinhos_aux->prox_vizinho;
		}
		
		//verifica se vizinho foi encontrado na lista
		if(vizinhos_aux!=NULL)
		{
			//remover primeiro vizinho
			if(vizinhos_ant==NULL)
			{
				grafo_aux->vizinhos= vizinhos_aux->prox_vizinho;
			}
			//remover vizinho intermediario
			else
			{
				vizinhos_ant->prox_vizinho=vizinhos_aux->prox_vizinho;
			}
			
			grafo_aux->grau_nodo--;
			
		}
		else
		{
			printf("erro remocao vizinhos\n");
		}
		
		
		vizinhos_nodo_remover=vizinhos_nodo_remover->prox_vizinho;
		
	}
	
	return grafo;
}

/**
 * Remove um nodo de um grafo, assim como todas suas ocorrências como vizinhos de outros nodos
 * name: grafo_remove
 * @param grafo: onde se deve remover um nodo
 * @param id_nodo_remover: qual id deve ser removido
 * @return Grafo atualizado
 * 
 */
comp_graph_t* grafo_remove(comp_graph_t* grafo, int id_nodo_remover)
{
	comp_graph_t* grafo_aux = grafo;
	
	
	//se grafo está vazio, não tem nada para remover
	if(grafo_aux == NULL)
	{
		return grafo;
	}
	else
	{		
		//procura o nodo a ser removido
		
		comp_graph_t* grafo_ant = NULL;
		while(grafo_aux!= NULL && grafo_aux->id_nodo!=id_nodo_remover)
		{
			//salva nodo anterior para usar na remoção
			grafo_ant = grafo_aux;
			grafo_aux = grafo_aux->prox_nodo;
		}
		
		//verifica se o nodo a ser removido foi encontrado
			//nodo a ser removido foi encontrado
		if(grafo_aux!=NULL)
		{
			//FAZ REMOÇÃO DO NODO NA LISTA DO GRAFO
			//nodo a ser removido é o primeiro da lista
			if(grafo_ant == NULL)
			{
				grafo = grafo_aux->prox_nodo;
			}
			//nodo a ser removido não é o primeiro da lista --> atualizar prox-nodo do anterior como o proximo do que será removido
			else
			{
				grafo_ant->prox_nodo = grafo_aux->prox_nodo;
			}
			
			//FAZ A REMOÇÃO DO NODO NA LISTA DE VIZINHOS DOS OUTROS NODOS
			grafo_remove_vizinho(grafo,id_nodo_remover,grafo_aux->vizinhos);
			
			return grafo;
		}
			//nodo a ser removido não foi encontrado, retorna o grafo sem alterações
		else
		{
			return grafo;
		}
	}
}

/**
 * Atualiza lista de vizinhos de um nodo, adicionando novo vizinho
 * name: grafo_insere_vizinho
 * @param grafo: onde deve ser inserido novo nodo vizinho
 * @param id_nodo: qual nodo terá adicionado um vizinho
 * @param id_vizinho: qual nodo será vizinho
 * @return Grafo atualizado
 * 
 */
comp_graph_t* grafo_insere_vizinho(comp_graph_t* grafo, int id_nodo, int id_vizinho)
{
	comp_graph_t* grafo_aux = grafo;
	
	Vizinho* novo;
    novo=(Vizinho*)malloc(sizeof(Vizinho));
    novo->id_nodo = id_vizinho;
    novo->prox_vizinho = NULL;
	
	//procura nodo 1 para inserir nodo 2 na lista de vizinhos do nodo 1
	while(grafo_aux!= NULL && grafo_aux->id_nodo!=id_nodo)
	{
		grafo_aux=grafo_aux->prox_nodo;
	}
	
	if(grafo_aux!=NULL)
	{
		Vizinho* viz_aux = grafo_aux->vizinhos;
		
		//nodo possui pelo menos um vizinho já incluso
		if(viz_aux!=NULL)
		{
			//procura ultimo vizinho incluso
			while(viz_aux->prox_vizinho!=NULL)
			{
				viz_aux=viz_aux->prox_vizinho;
			}
			
			viz_aux->prox_vizinho=novo;
		}
		//nodo não possui vizinhos
		else
		{
			grafo_aux->vizinhos = novo;
		}
		
		grafo_aux->grau_nodo++;
		
	}
	else
	{
		printf("NODO NÃO ENCONTRADO -- ERRO : %d\n",id_nodo);
	}
	
	return grafo;
}


/**
 * Conecta dois nodos já existentes, atualizando a lista de vizinhos de cada um deles
 * name: grafo_conecta_nodos
 * @param grafo: onde serão conectados os nodos
 * @param id_nodo_1: um dos nodos envolvidos na conexão
 * @param id_nodo_2: outro nodo envolvido na conexão
 * @return Grafo atualizado
 * 
 */
comp_graph_t* grafo_conecta_nodos(comp_graph_t* grafo, int id_nodo_1, int id_nodo_2)
{
	grafo = grafo_insere_vizinho(grafo,id_nodo_1,id_nodo_2);
	grafo = grafo_insere_vizinho(grafo,id_nodo_2,id_nodo_1);	
	
	return grafo;
}

/**
 * Cria um novo nodo para um grafo, dado seu id e com grau zero (sem vizinhos)
 * name: grafo_novo
 * @param id_nodo: id do novo nodo a ser criado
 * @param conteudo_nodo: conteudo do novo nodo a ser criado
 * @return Ponteiro para um nodo tipo grafo
 * 
 */
comp_graph_t* grafo_novo(int id_nodo, Conteudo* conteudo_nodo)
{
	comp_graph_t* novo;
    novo=(comp_graph_t*)malloc(sizeof(comp_graph_t));
	
	novo->id_nodo=id_nodo;
	novo->conteudo_nodo = conteudo_nodo;
	novo->grau_nodo=0;
	novo->vizinhos=NULL;
	novo->prox_nodo=NULL;
	
	return novo;
}

/**
 * Insere um novo nodo num grafo
 * name: grafo_insere_nodo
 * @param grafo: onde deve ser inserido novo nodo
 * @param id_nodo_novo: id do novo nodo a ser criado
 * @param conteudo_nodo: conteudo do novo nodo a ser criado
 * @return Grafo atualizado
 * 
 */
comp_graph_t* grafo_insere_nodo(comp_graph_t* grafo, int id_nodo_novo, Conteudo* conteudo_nodo)
{
	comp_graph_t* grafo_aux = grafo;
	
	comp_graph_t* novo = grafo_novo(id_nodo_novo, conteudo_nodo);
	
	if(grafo_aux!=NULL)
	{
		//percorre até o final da lista dos nodos
		while(grafo_aux->prox_nodo!=NULL)
		{
			grafo_aux = grafo_aux->prox_nodo;
		}
		
		grafo_aux->prox_nodo = novo;
	}
	//grafo está vazio, primeiro a inserir
	else
	{
		return novo;
	}
	
	return grafo;
}


/**
 * Altera o conteudo de um nodo já existente
 * name: grafo_altera_conteudo_nodo
 * @param grafo: onde será alterado conteudo de um nodo
 * @param id_nodo: id do nodo que será alterado
 * @param novo_conteudo: novo conteudo para o nodo com id_nodo
 * @return Grafo atualizado
 * 
 */
comp_graph_t* grafo_altera_conteudo_nodo(comp_graph_t* grafo, int id_nodo, Conteudo* novo_conteudo)
{
	comp_graph_t* grafo_aux= grafo;
	
	//procura id do nodo a ser alterado
	while(grafo_aux!=NULL && grafo_aux->id_nodo!=id_nodo)
	{
		grafo_aux= grafo_aux->prox_nodo;
	}
	
	//id do nodo foi encontrado
	if(grafo_aux!=NULL)
	{
		grafo_aux->conteudo_nodo=novo_conteudo;
	}
	else
	{
		printf("Id nodo nao encontrado %d\n", id_nodo);
	}
	
	return grafo;
}

/**
 * Imprime na tela (printf) conteudo de um grafo
 * name: grafo_imprime
 * @param grafo: qual grafo será impresso
 * @return 
 * 
 */
void grafo_imprime(comp_graph_t* grafo)
{
	comp_graph_t* aux = grafo;
	printf("IMPRIMINDO GRAFO\n");
	while(aux!=NULL)
	{
		printf("\nNodo id: %d\n", aux->id_nodo);
		printf("Nodo conteudo: %s\n", aux->conteudo_nodo->info);
		printf("Nodo grau: %d\n", aux->grau_nodo);
		
		//imprime vizinhos
		Vizinho* aux_v = aux->vizinhos;
		while(aux_v!=NULL)
		{
			printf("Id vizinho: %d\n", aux_v->id_nodo);
			aux_v=aux_v->prox_vizinho;
		}
		
		aux = aux->prox_nodo;
	}
	printf("\nFIM GRAFO\n\n");
}


