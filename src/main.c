/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include <stdlib.h>
#include "comp_dict.h"
#include "comp_list.h"
#include "comp_tree.h"
#include "comp_graph.h"
extern comp_dict_t* dicionario;
extern int linha;

void yyerror (char const *mensagem)
{
  fprintf (stderr, "%s na linha %d\n", mensagem, linha);
}

int main (int argc, char **argv)
{
  int resultado = yyparse();

	//dicionario_imprime(dicionario);
  return resultado;
}
