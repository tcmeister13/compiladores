/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include <stdlib.h>
#include "comp_tree.h"

/**
 * Cria uma nova �rvore
 * name: arvore_inicializa
 * @param
 * @return NULL
 * 
 */
comp_tree_t* arvore_inicializa(){
    return NULL;
}


/**
 * Cria um elemento com as informa��es do nodo 
 * name: arvore_cria_infO
 * @param identificador: id do novo elemento a ser criado
 * @return novo TipoInfo_a
 * 
 */
 TipoInfo_a* arvore_cria_info(int identificador){
    TipoInfo_a *novo;
    novo = (TipoInfo_a*) malloc(sizeof(TipoInfo_a));

    novo->id = identificador;
    return novo;
}


/**
 * Remove nodo da �rvore com referido id
 * name: arvore_remove_por_id
 * @param arv: onde ser� removido nodo
 * @param identificador: qual elemento ser� removido
 * @return Arvore atualizada
 * 
 */
comp_tree_t* arvore_remove_por_id(comp_tree_t* arv, int identificador){
    TipoFilhos *aux_filhos;
    TipoFilhos *anterior;
    if(arv == NULL){return NULL;} /* arvore vazia */
    if(arv->nodo->id == identificador){ /* estou no nodo com o id */
        free(arv);
        printf("Nodo %d encontrado e removido!\n", identificador);
        return NULL;
    }
    /* testar cada filho, se tiver o id, remove da lista de filhos */
    aux_filhos = arv->filhos;
    anterior = aux_filhos;
    if(aux_filhos == NULL){ return arv; }
    /* se for o primeiro filho */
    if(aux_filhos->arv->nodo->id == identificador){
        arv->filhos = arv->filhos->prox;
        printf("Nodo %d encontrado e removido!\n", identificador);
        return arv;
    }
    while(aux_filhos != NULL){
        if(aux_filhos->arv->nodo->id == identificador){
            anterior->prox = aux_filhos->prox;
            free(aux_filhos);
            printf("Nodo %d encontrado e removido!\n", identificador);
            return arv;
        }
        anterior = aux_filhos;
        aux_filhos = aux_filhos->prox;
    }
    /* se n�o foi nenhum dos filhos, testar para todos os filhos (recurs�o) */
    while(aux_filhos != NULL){
        aux_filhos->arv = (aux_filhos->arv, identificador);
        aux_filhos = aux_filhos->prox;
    }
    return arv;
}


/**
 * Insere um novo filho no nodo recebido
 * name: arvore_insere_filho
 * @param arv: onde ser� inserido novo filho
 * @param novo: novo filho a ser inserido
 * @return Arvore atualizada
 * 
 */
comp_tree_t* arvore_insere_filho(comp_tree_t* arv, TipoInfo_a* novo){
    /* Se for NULL significa que a arvora nao existe, raiz vazia*/
    if(arv == NULL){
        arv = (comp_tree_t*) malloc(sizeof(comp_tree_t));
        arv->filhos = NULL;
        arv->nodo = novo;
        return arv;
    }
    /* cria novo filho */
    TipoFilhos *filho_n = (TipoFilhos*) malloc(sizeof(TipoFilhos));
    filho_n->arv = (comp_tree_t*) malloc(sizeof(comp_tree_t));
    filho_n->arv->nodo = novo;
    filho_n->arv->filhos = NULL;
    filho_n->prox = NULL;
    /*se n�o, insere um novo filho no pai */
    TipoFilhos *ptaux = arv->filhos;
    if(ptaux == NULL) { /* nodo ainda n�o tem filhos */
        arv->filhos = filho_n;
        return arv;
    }
    /* nodo ja tem filhos ent�o porcura e insere no fim */
    while(ptaux->prox != NULL){
        ptaux = ptaux->prox;
    }
    ptaux->prox = filho_n;
    return arv;
}


/**
 * Imprime na tela arvore conforme profundidade a esquerda
 * name: arvore_imprime_profundidade_a_esquerda
 * @param arv: arvore a ser impressa
 * @return 
 * 
 */
void arvore_imprime_profundidade_a_esquerda(comp_tree_t* arv){
    TipoFilhos *filhos;
    if(arv == NULL){
        printf("Arvore vazia\n");
        return;
    }
    filhos = arv->filhos;
    printf("%d\n", arv->nodo->id);
    while(filhos != NULL){
        arvore_imprime_profundidade_a_esquerda(filhos->arv);
        filhos = filhos->prox;
    }
    return;
}

/**
 * Imprime arvore (printf) na tela
 * name: arvore_imprime_visual
 * @param arv: arvore a ser impressa
 * @return 
 * 
 */
void arvore_imprime_visual(comp_tree_t* arv){
    TipoFilhos *filhos;
    /* se nullo, imprime null */
    if(arv == NULL){
        printf("Arvore vazia\n");
        return;
    }
    filhos = arv->filhos;
    /* imprime dados do nodo atual */
    printf("Nodo: %d\n", arv->nodo->id);
    /* se nodo sem filhos */
    if(arv->filhos == NULL){
        printf("\tnull\n");
        return;
    }
    /* imprime id filhos */
    while(filhos != NULL){
        printf("\t%d\n", filhos->arv->nodo->id);
        filhos = filhos->prox;
    }
    /* chama recurs�o para cada filho */
    filhos = arv->filhos;
    while(filhos != NULL){
        arvore_imprime_visual(filhos->arv);
        filhos = filhos->prox;
    }
    return;
}

