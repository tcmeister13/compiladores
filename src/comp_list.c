/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include <stdlib.h>
#include "comp_list.h"


/**
 * Cria nova lista
 * name: lista_inicializa
 * @param
 * @return NULL
 * 
 */
comp_list_t* lista_inicializa(){
    return NULL;
}


/**
 * Cria um elemento da lista com os dados
 * name: lista_cria_info
 * @param identificador: id do novo elemento da lista
 * @return Novo TipoInfo_l
 * 
 */
TipoInfo_l* lista_cria_info(int identificador){
    TipoInfo_l *novo;
    novo = (TipoInfo_l*) malloc(sizeof(TipoInfo_l));

    novo->id = identificador;
    return novo;
}

/**
 * Insere um novo elemento no fim da lista
 * name: lista_insere_fim
 * @param l: onde ser� inserido nova informa��o
 * @param infos: informa��es do novo elemento da lista
 * @return Lista atualizada
 * 
 */
comp_list_t* lista_insere_fim(comp_list_t* l, TipoInfo_l* infos){
       comp_list_t *novo;      /* novo elemento */
       comp_list_t *ptaux = l; /* ponteiro auxiliar para percorrer a lista */

       /* aloca um novo nodo */
       novo = (comp_list_t*) malloc(sizeof(comp_list_t));

       /*insere a informa��o no novo nodo*/
       novo->prox = NULL;
       novo->nodo = infos;

       /*lista ainda nao existe, cria uma lista com o elemento inserido*/
       if (l == NULL){
               l = novo;
               return l;
       }
       /*procurando a posi��o de inser��o*/
       while(ptaux->prox != NULL){
           ptaux = ptaux->prox;
       }
       /* Achou posi��o */
       ptaux->prox = novo;
       return l;
}


/**
 * Remove primeiro elemento da lista
 * name: lista_remove_primeiro
 * @param l: onde ser� removido primeiro elemento
 * @return Lista atualizada
 * 
 */
comp_list_t* lista_remove_primeiro(comp_list_t* l){
     if(l == NULL){
        printf("Lista vazia!\n");
        return NULL;
     }
     return l->prox;
}

/**
 * Concatena duas listas, lista 2 ao fim da lista 1
 * name: lista_concatena
 * @param l1: primeira lista
 * @param l2: segunda lista
 * @return Lista contendo l1 e l2 concatenadas
 * 
 */
comp_list_t* lista_concatena(comp_list_t* l1, comp_list_t* l2){
    comp_list_t *ptaux = l1;
    if(l1 == NULL){
        return l2;
    }
    while(ptaux->prox != NULL){
        ptaux = ptaux->prox;
    }
    /* Achou posi��o */
    ptaux->prox = l2;
    return l1;
}

/**
 * Imprime na tela (printf) conteudo de um lista
 * name: lista_imprime
 * @param l: lista que ser� impressa
 * @return 
 * 
 */
void lista_imprime(comp_list_t* l){
    comp_list_t *ptaux = l;
    printf("\n --- LISTA: ---\n");
    while(ptaux != NULL){
        printf("%d\n", ptaux->nodo->id);
        ptaux = ptaux->prox;
    }
    printf("----------- \n");
    return;
}


/**
 * Percorre lista buscando o elemento pelo id - quando encontra, remove elemento da lista
 * name: lista_remove_por_id
 * @param l: onde ser� removido elemento
 * @param identificador: id do elemento da lista que ser� removido
 * @return Lista atualizada
 * 
 */
comp_list_t* lista_remove_por_id(comp_list_t* l, int identificador){
    comp_list_t* ptaux = l;
    comp_list_t* retorno = l;
    comp_list_t* anterior = l;
    if (ptaux == NULL){//Lista Vazia
       printf("Lista vazia!\n");
       return NULL; //retorna a lista original
    }
    if (ptaux->nodo->id == identificador){//� o priemiro da lista
        retorno = ptaux->prox;
        free(ptaux);
        return retorno;
    }

    do{//encontra elemento
        anterior = ptaux;
        ptaux = ptaux->prox;
        if(ptaux == NULL){ /* nao encontrou */
            printf("Elemento %d nao encontrado!\n", identificador);
            return l;
        }
    }while (ptaux->nodo->id != identificador);
    anterior->prox=ptaux->prox;
    free(ptaux);
    return l;
}


