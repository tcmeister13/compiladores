/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/

typedef struct TIPO_DICIONARIO comp_dict_t;


//tipo dicionario: lista encadeada para entradas
struct TIPO_DICIONARIO
{
    char *chave;
    int dado;
    int tipo;
    comp_dict_t * prox;
};

//métodos de manipulação de entradas
void dicionario_imprime(comp_dict_t* dicio);
int dicionario_chave_nao_existente(char* Chave, comp_dict_t* Dicionario);

//métodos de manipulação do dicionário
comp_dict_t* dicionario_Criar ();
comp_dict_t* dicionario_AdicionarEntrada (char* Chave, int dado, comp_dict_t* Dicionario);
comp_dict_t* dicionario_RemoverEntrada (char* Chave, comp_dict_t* Dicionario);
comp_dict_t* dicionario_AlterarEntrada (char* Chave, int dado, comp_dict_t* Dicionario);
comp_dict_t* dicionario_AdicionaTipo (char* Chave, int tipo, comp_dict_t* Dicionario);
comp_dict_t* dicionario_LocalizarEntrada (char* Chave, comp_dict_t* Dicionario);

