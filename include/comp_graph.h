/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
typedef struct TIPO_VIZINHO Vizinho;
typedef struct TIPO_GRAFO comp_graph_t;
typedef struct TIPO_INFO_NODO Conteudo;

struct TIPO_VIZINHO
{
    int id_nodo;
    Vizinho *prox_vizinho;
};

struct TIPO_INFO_NODO
{
	char *info;
};

struct TIPO_GRAFO
{
    int id_nodo;
    Conteudo* conteudo_nodo;
    int grau_nodo;
    Vizinho *vizinhos;
    comp_graph_t *prox_nodo;
};

comp_graph_t* grafo_remove_vizinho(comp_graph_t* grafo, int id_nodo_remover, Vizinho* vizinhos_nodo_remover);
comp_graph_t* grafo_insere_vizinho(comp_graph_t* grafo, int id_nodo, int id_vizinho);
comp_graph_t* grafo_novo(int id_nodo, Conteudo* conteudo_nodo);
void grafo_imprime(comp_graph_t* grafo);

comp_graph_t* grafo_criacao();
comp_graph_t* grafo_remove(comp_graph_t* grafo, int id_nodo_remover);
comp_graph_t* grafo_conecta_nodos(comp_graph_t* grafo, int id_nodo_1, int id_nodo_2);
comp_graph_t* grafo_insere_nodo(comp_graph_t* grafo, int id_nodo_novo, Conteudo* conteudo_nodo);
comp_graph_t* grafo_altera_conteudo_nodo(comp_graph_t* grafo, int id_nodo, Conteudo* novo_conteudo);
