/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
/*
Especifica��o:
    - Cria��o
    - Remo��o
    - Altera��o
    - Inser��o
    - N-�ria
*/

typedef struct arvore comp_tree_t;
typedef struct filho TipoFilhos;
typedef struct info_a TipoInfo_a;

struct arvore{
    TipoInfo_a  *nodo;    /* caracter�sticas de um nodo da lista */
    TipoFilhos  *filhos;  /* encadeia para proximo elemento da lista */
};

struct filho{
    comp_tree_t  *arv;     /* ponteiro para o nodo da arvore */
    TipoFilhos  *prox;    /* encadeia para proximo elemento da lista */
};

struct info_a{
    /* Os atributos do nodo ser�o criados ao decorrer do projeto */
    int id;     /* atributo de busca de item na lista tempor�rio */
};



/* Manipula��o da arvore */
comp_tree_t* arvore_inicializa();
TipoInfo_a* arvore_cria_info(int identificador);
comp_tree_t* arvore_remove_por_id(comp_tree_t* arv, int identificador); /* Remove nodo da �rvore com referido id */
comp_tree_t* arvore_insere_filho(comp_tree_t* arv, TipoInfo_a* novo); /* insere um novo filho no nodo recebido */
void        arvore_imprime_profundidade_a_esquerda(comp_tree_t* arv);
void        arvore_imprime_visual(comp_tree_t* arv);
