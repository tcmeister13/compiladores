var searchData=
[
  ['iks_5fsimbolo_5fidentificador',['IKS_SIMBOLO_IDENTIFICADOR',['../parser_8c.html#ae12bced70d7ab3294fff8db97d062daf',1,'parser.c']]],
  ['iks_5fsimbolo_5findefinido',['IKS_SIMBOLO_INDEFINIDO',['../parser_8c.html#ad3e2e21fca1e68569e583d1340290526',1,'parser.c']]],
  ['iks_5fsimbolo_5fliteral_5fbool',['IKS_SIMBOLO_LITERAL_BOOL',['../parser_8c.html#aa4f468b05c912ff12ec7c73e2e95dbda',1,'parser.c']]],
  ['iks_5fsimbolo_5fliteral_5fchar',['IKS_SIMBOLO_LITERAL_CHAR',['../parser_8c.html#ad93951012ae2a818ba07539efffbf444',1,'parser.c']]],
  ['iks_5fsimbolo_5fliteral_5ffloat',['IKS_SIMBOLO_LITERAL_FLOAT',['../parser_8c.html#a3cc56aba35d5c415d90b19f41dc5ec4f',1,'parser.c']]],
  ['iks_5fsimbolo_5fliteral_5fint',['IKS_SIMBOLO_LITERAL_INT',['../parser_8c.html#a25a7443cdeabca76f4c7526041a69933',1,'parser.c']]],
  ['iks_5fsimbolo_5fliteral_5fstring',['IKS_SIMBOLO_LITERAL_STRING',['../parser_8c.html#a895f1647b277c562c8a1e3270109cf97',1,'parser.c']]],
  ['initial',['INITIAL',['../lexer_8c.html#aa3d063564f6ab16f6d408b8369d0e9ff',1,'lexer.c']]],
  ['int16_5fmax',['INT16_MAX',['../lexer_8c.html#ac58f2c111cc9989c86db2a7dc4fd84ca',1,'lexer.c']]],
  ['int16_5fmin',['INT16_MIN',['../lexer_8c.html#ad4e9955955b27624963643eac448118a',1,'lexer.c']]],
  ['int32_5fmax',['INT32_MAX',['../lexer_8c.html#a181807730d4a375f848ba139813ce04f',1,'lexer.c']]],
  ['int32_5fmin',['INT32_MIN',['../lexer_8c.html#a688eb21a22db27c2b2bd5836943cdcbe',1,'lexer.c']]],
  ['int8_5fmax',['INT8_MAX',['../lexer_8c.html#aaf7f29f45f1a513b4748a4e5014ddf6a',1,'lexer.c']]],
  ['int8_5fmin',['INT8_MIN',['../lexer_8c.html#aadcf2a81af243df333b31efa6461ab8e',1,'lexer.c']]]
];
